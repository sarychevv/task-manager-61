package ru.t1.sarychevv.tm.service.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.model.IRepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.model.IService;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.field.IdEmptyException;
import ru.t1.sarychevv.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected IConnectionService connectionService;

    @Autowired
    protected ApplicationContext context;

    @Getter
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract IRepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) {
        return getRepository().save(model);
    }

    @Override
    @Transactional
    public void removeAll() {
        getRepository().deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsById(id);
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findById(id).orElse(null);
    }

    @NotNull
    @Override
    public Integer getSize() {
        return Math.toIntExact(getRepository().count());
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        getRepository().delete(model);
    }

    @Override
    public void removeOneById(@Nullable final String id) {
        @Nullable M result = findOneById(id);
        if (result == null) throw new ModelNotFoundException();
        getRepository().delete(result);
    }

    @NotNull
    @Override
    @Transactional
    public M update(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return getRepository().save(model);
    }

    @Nullable
    public List<M> findAll(@Nullable Sort sort) {
        if (sort == null) return findAll();
        return getRepository().findAll(sort);
    }

}

