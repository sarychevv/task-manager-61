package ru.t1.sarychevv.tm.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.dto.IDTORepository;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.dto.IDTOService;
import ru.t1.sarychevv.tm.dto.model.AbstractModelDTO;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.field.IdEmptyException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Service
@NoArgsConstructor
public abstract class AbstractDTOService<M extends AbstractModelDTO, R extends IDTORepository<M>> implements IDTOService<M> {

    @NotNull
    @Autowired
    protected IConnectionService connectionService;

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract IDTORepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@NotNull final M model) {
        getRepository().save(model);
        return model;
    }

    @Override
    @Transactional
    public void removeAll() {
        getRepository().deleteAll();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsById(id);
    }

    @Nullable
    @Override
    public List<M> findAll() {
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Comparator comparator) {
        if (comparator == null) return findAll();
        return (List<M>) getRepository().findAll().stream().sorted(comparator).collect(Collectors.toList());
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        return getRepository().findAll(sort);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findById(id).orElse(null);
    }

    @Override
    public Integer getSize() {
        return Math.toIntExact(getRepository().count());
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        getRepository().delete(model);
    }

    @Override
    @Transactional
    public void removeOneById(@Nullable final String id) {
        @Nullable M result = findOneById(id);
        if (result == null) throw new ModelNotFoundException();
        getRepository().delete(result);
    }

    @NotNull
    @Override
    @Transactional
    public M update(@Nullable final M model) {
        if (model == null) throw new ModelNotFoundException();
        return getRepository().save(model);
    }

}

