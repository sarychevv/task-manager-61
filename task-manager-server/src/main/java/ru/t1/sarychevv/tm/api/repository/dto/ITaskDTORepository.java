package ru.t1.sarychevv.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;

import java.util.List;

@Repository
public interface ITaskDTORepository extends IUserOwnedDTORepository<TaskDTO> {

    List<TaskDTO> findAllByUserIdAndProjectId(@Nullable String userId,
                                              @NotNull String projectId);

}

