package ru.t1.sarychevv.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.sarychevv.tm.enumerated.OperationType;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    private OperationType type;

    private Object entity;

    private String table;

    private long timestamp = System.currentTimeMillis();

    public OperationEvent(final OperationType type, final Object entity) {
        this.type = type;
        this.entity = entity;
    }

}
