package ru.t1.sarychevv.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {

    @NotNull
    ProjectDTO create(@Nullable String userId,
                      @Nullable String name);

    @NotNull
    ProjectDTO create(@Nullable String userId,
                      @Nullable String name,
                      @Nullable String description);

}

