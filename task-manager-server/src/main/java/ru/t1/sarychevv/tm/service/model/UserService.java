package ru.t1.sarychevv.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.model.IUserRepository;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.api.service.model.IUserService;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.exception.user.ExistsEmailException;
import ru.t1.sarychevv.tm.exception.user.ExistsLoginException;
import ru.t1.sarychevv.tm.exception.user.UserNotFoundException;
import ru.t1.sarychevv.tm.model.User;
import ru.t1.sarychevv.tm.util.HashUtil;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserRepository repository;


    @NotNull
    protected IUserRepository getRepository() {
        return repository;
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login,
                       @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPassword(HashUtil.salt(propertyService, password));
        return repository.save(user);
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login,
                       @Nullable final String password,
                       @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPassword(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        return repository.save(user);
    }

    @NotNull
    @Override
    @Transactional
    public User create(@Nullable final String login,
                       @Nullable final String password,
                       @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull User user = new User();
        user.setLogin(login);
        user.setPassword(HashUtil.salt(propertyService, password));
        user.setRole(role);
        return repository.save(user);
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    public User findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    public Boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return (repository.findByLogin(login) != null);
    }

    @Override
    public Boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return (repository.findByEmail(email) != null);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.save(user);
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        removeOne(user);
    }

    @Override
    @Transactional
    public void setPassword(@Nullable final String id,
                            @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPassword(HashUtil.salt(propertyService, password));
        repository.save(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.save(user);
    }

    @Override
    @Transactional
    public void updateUser(@Nullable final String id,
                           @Nullable final String firstName,
                           @Nullable final String lastName,
                           @Nullable final String middleName) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName((firstName == null) ? "" : firstName);
        user.setLastName((lastName == null) ? "" : lastName);
        user.setMiddleName((middleName == null) ? "" : middleName);
        repository.save(user);
    }

}

