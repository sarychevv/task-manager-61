package ru.t1.sarychevv.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.sarychevv.tm.component.Bootstrap;
import ru.t1.sarychevv.tm.configuration.ServerConfiguration;

public class Application {

    public static void main(String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}
