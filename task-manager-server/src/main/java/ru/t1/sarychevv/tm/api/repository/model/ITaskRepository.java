package ru.t1.sarychevv.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.model.Task;

import java.util.List;

@Repository
public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @Nullable
    List<Task> findAllByUserIdAndProjectId(@Nullable String userId,
                                           @NotNull String projectId);

}
