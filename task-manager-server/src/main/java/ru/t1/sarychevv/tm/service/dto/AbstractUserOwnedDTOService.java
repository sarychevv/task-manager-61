package ru.t1.sarychevv.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.sarychevv.tm.api.service.dto.IUserOwnedDTOService;
import ru.t1.sarychevv.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.entity.ProjectNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;

import java.util.List;

@Service
@NoArgsConstructor
public abstract class AbstractUserOwnedDTOService<M extends AbstractUserOwnedModelDTO, R extends IUserOwnedDTORepository<M>>
        extends AbstractDTOService<M, R> implements IUserOwnedDTOService<M> {

    @NotNull
    protected abstract IUserOwnedDTORepository<M> getRepository();

    @NotNull
    @Override
    @Transactional
    public M add(@Nullable final String userId,
                 @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelEmptyException();
        return getRepository().save(model);
    }

    @Override
    @Transactional
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        getRepository().deleteById(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId,
                              @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) return false;
        return getRepository().existsById(id);
    }

    @Nullable
    @Override
    public List<M> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return getRepository().findAll();
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId,
                         @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return getRepository().findById(id).orElse(null);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId,
                            @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final List<M> models = getRepository().findAllByUserId(userId);
        if (models == null) throw new ProjectNotFoundException();
        return models.get(index);
    }

    @NotNull
    @Override
    public Integer getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        return Math.toIntExact(getRepository().count());
    }

    @Override
    @Transactional
    public void removeOne(@Nullable final String userId,
                          @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        getRepository().delete(model);
    }

    @NotNull
    @Override
    public M removeOneById(@Nullable final String userId,
                           @Nullable final String id) {
        @Nullable M result = findOneById(userId, id);
        removeOne(userId, result);
        return result;
    }

    @Nullable
    @Override
    public M removeOneByIndex(@Nullable final String userId,
                              @Nullable final Integer index) {
        @Nullable M result = findOneByIndex(userId, index);
        removeOne(userId, result);
        return result;
    }

    @Override
    @Transactional
    public void update(@Nullable final String userId,
                       @Nullable final M model) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) return;
        getRepository().save(model);
    }

    @NotNull
    @Override
    @Transactional
    public M changeStatusById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final Status status) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @Nullable final M model = findOneById(userId, id);
        if (model == null) throw new ModelNotFoundException();
        if (status == null) throw new StatusEmptyException();
        getRepository().save(model);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public M changeStatusByIndex(@Nullable final String userId,
                                 @Nullable final Integer index,
                                 @Nullable final Status status) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        @Nullable final M model = findOneByIndex(userId, index);
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (model == null) throw new ModelNotFoundException();
        if (status == null) throw new StatusEmptyException();
        getRepository().save(model);
        return model;
    }
}

