package ru.t1.sarychevv.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.enumerated.Status;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> {

    boolean existsById(@Nullable String userId,
                       @Nullable String id);

    @Nullable List<M> findAll(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId,
                    @Nullable Sort sort);

    @Nullable
    M findOneById(@Nullable String userId,
                  @Nullable String id);

    @Nullable
    M findOneByIndex(@Nullable String userId,
                     @Nullable Integer index);

    @NotNull
    Integer getSize(@Nullable String userId);

    @Transactional
    void removeOne(@Nullable String userId,
                   @Nullable M model);

    @Nullable
    @Transactional
    M removeOneById(@Nullable String userId,
                    @Nullable String id);

    @Nullable
    @Transactional
    M removeOneByIndex(@Nullable String userId,
                       @Nullable Integer id);

    @Transactional
    M add(@Nullable String userId, @Nullable M model);

    @Transactional
    void removeAll(@Nullable String userId);

    @Transactional
    void update(@Nullable String userId,
                @Nullable M model);

    @NotNull
    @Transactional
    M changeStatusById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable Status status);

    @NotNull
    @Transactional
    M changeStatusByIndex(@Nullable String userId,
                          @Nullable Integer index,
                          @Nullable Status status);

    @NotNull
    @Transactional
    M updateById(@Nullable String userId,
                 @Nullable String id,
                 @Nullable String name,
                 @Nullable String description);

    @NotNull
    @Transactional
    M updateByIndex(@Nullable String userId,
                    @Nullable Integer index,
                    @Nullable String name,
                    @Nullable String description);
}

