package ru.t1.sarychevv.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.model.AbstractModel;

import java.util.List;

@NoRepositoryBean
public interface IRepository<M extends AbstractModel> extends JpaRepository<M, String> {
    @NotNull
    List<M> findAll(@NotNull final Sort sort);
}

