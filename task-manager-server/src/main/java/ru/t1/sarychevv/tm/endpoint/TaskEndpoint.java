package ru.t1.sarychevv.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.sarychevv.tm.api.endpoint.ITaskEndpoint;
import ru.t1.sarychevv.tm.api.service.dto.IProjectTaskDTOService;
import ru.t1.sarychevv.tm.api.service.dto.ITaskDTOService;
import ru.t1.sarychevv.tm.dto.model.SessionDTO;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;
import ru.t1.sarychevv.tm.dto.request.task.*;
import ru.t1.sarychevv.tm.dto.response.task.*;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.enumerated.TaskSort;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@Controller
@NoArgsConstructor
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.sarychevv.tm.api.endpoint.ITaskEndpoint")
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    @Autowired
    private IProjectTaskDTOService projectTaskDTOService;

    @Autowired
    private ITaskDTOService taskDTOService;

    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull final TaskBindToProjectRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable TaskDTO task = projectTaskDTOService.bindTaskToProject(userId, projectId, taskId);
        return new TaskBindToProjectResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(@WebParam(name = REQUEST, partName = REQUEST)
                                                               @NotNull final TaskUnbindFromProjectRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable final String taskId = request.getTaskId();
        @Nullable TaskDTO task = projectTaskDTOService.unbindTaskFromProject(userId, projectId, taskId);
        return new TaskUnbindFromProjectResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskChangeStatusByIdResponse changeTaskStatusById(@WebParam(name = REQUEST, partName = REQUEST)
                                                             @NotNull final TaskChangeStatusByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final Status status = request.getStatus();
        @Nullable TaskDTO task = taskDTOService.changeStatusById(userId, id, status);
        return new TaskChangeStatusByIdResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskChangeStatusByIndexResponse changeTaskStatusByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                                   @NotNull final TaskChangeStatusByIndexRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final Status status = request.getStatus();
        @Nullable TaskDTO task = taskDTOService.changeStatusByIndex(userId, index, status);
        return new TaskChangeStatusByIndexResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskClearResponse clearTask(@WebParam(name = REQUEST, partName = REQUEST)
                                       @NotNull final TaskClearRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        taskDTOService.removeAll(userId);
        return new TaskClearResponse();
    }

    @Override
    @NotNull
    @WebMethod
    public TaskCreateResponse createTask(@WebParam(name = REQUEST, partName = REQUEST)
                                         @NotNull final TaskCreateRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable TaskDTO task = taskDTOService.create(userId, name, description);
        return new TaskCreateResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskGetByIdResponse getTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                           @NotNull final TaskGetByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable TaskDTO task = taskDTOService.findOneById(userId, id);
        return new TaskGetByIdResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskGetByIndexResponse getTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                 @NotNull final TaskGetByIndexRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable TaskDTO task = taskDTOService.findOneByIndex(userId, index);
        return new TaskGetByIndexResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskListByProjectIdResponse getTaskByProjectId(@WebParam(name = REQUEST, partName = REQUEST)
                                                          @NotNull final TaskListByProjectIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String projectId = request.getProjectId();
        @Nullable List<TaskDTO> tasks = taskDTOService.findAllByProjectId(userId, projectId);
        return new TaskListByProjectIdResponse(tasks);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskListResponse listTask(@WebParam(name = REQUEST, partName = REQUEST)
                                     @NotNull final TaskListRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final TaskSort sort = request.getTaskSort();
        @Nullable List<TaskDTO> tasks = new ArrayList<>();
        tasks = taskDTOService.findAll(userId);
        return new TaskListResponse(tasks);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskRemoveByIdResponse removeTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                                 @NotNull final TaskRemoveByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable TaskDTO task = taskDTOService.removeOneById(userId, id);
        return new TaskRemoveByIdResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskRemoveByIndexResponse removeTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull final TaskRemoveByIndexRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable TaskDTO task = taskDTOService.removeOneByIndex(userId, index);
        return new TaskRemoveByIndexResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUpdateByIdResponse updateTaskById(@WebParam(name = REQUEST, partName = REQUEST)
                                                 @NotNull final TaskUpdateByIdRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String id = request.getId();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable TaskDTO task = taskDTOService.updateById(userId, id, name, description);
        return new TaskUpdateByIdResponse(task);
    }

    @Override
    @NotNull
    @WebMethod
    public TaskUpdateByIndexResponse updateTaskByIndex(@WebParam(name = REQUEST, partName = REQUEST)
                                                       @NotNull final TaskUpdateByIndexRequest request) {
        @NotNull final SessionDTO session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final Integer index = request.getIndex();
        @Nullable final String name = request.getName();
        @Nullable final String description = request.getDescription();
        @Nullable TaskDTO task = taskDTOService.updateByIndex(userId, index, name, description);
        return new TaskUpdateByIndexResponse(task);
    }

}
