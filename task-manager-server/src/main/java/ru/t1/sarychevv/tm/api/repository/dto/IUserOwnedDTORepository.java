package ru.t1.sarychevv.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.repository.NoRepositoryBean;
import ru.t1.sarychevv.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.enumerated.Sort;

import java.util.List;

@NoRepositoryBean
public interface IUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO> extends IDTORepository<M> {

    @Nullable
    M findByUserIdAndId(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAllByUserId(@Nullable String userId);

    int countByUserId(@Nullable String userId);

    @NotNull
    List<M> findByUser(@NotNull final UserDTO user, @NotNull final Sort sort);

}
