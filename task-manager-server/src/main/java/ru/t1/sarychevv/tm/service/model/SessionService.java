package ru.t1.sarychevv.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.sarychevv.tm.api.repository.model.ISessionRepository;
import ru.t1.sarychevv.tm.api.service.model.ISessionService;
import ru.t1.sarychevv.tm.api.service.model.IUserService;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.exception.entity.TaskNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.exception.user.UserNotFoundException;
import ru.t1.sarychevv.tm.model.Session;
import ru.t1.sarychevv.tm.model.User;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

    @NotNull
    @Autowired
    private IUserService userService;

    @NotNull
    protected ISessionRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    public List<Session> findAll(@Nullable final String userId,
                                 @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return repository.findAllByUserId(userId);
        @Nullable User user = userService.findOneById(userId);
        if (user == null) throw new UserNotFoundException();
        @Nullable List<Session> sessions = repository.findByUser(user, sort);
        return sessions;
    }

    @NotNull
    @Override
    @Transactional
    public Session updateById(@Nullable final String userId,
                              @Nullable final String id,
                              @Nullable final String name,
                              @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Session session = findOneById(userId, id);
        if (session == null) throw new TaskNotFoundException();
        session.setName(name);
        session.setDescription(description);
        repository.save(session);
        return session;
    }

    @NotNull
    @Override
    @Transactional
    public Session updateByIndex(@Nullable final String userId,
                                 @Nullable final Integer index,
                                 @Nullable final String name,
                                 @Nullable final String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final Session session = findOneByIndex(userId, index);
        if (session == null) throw new TaskNotFoundException();
        session.setName(name);
        session.setDescription(description);
        repository.save(session);
        return session;
    }

}

