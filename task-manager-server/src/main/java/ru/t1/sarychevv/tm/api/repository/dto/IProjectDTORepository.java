package ru.t1.sarychevv.tm.api.repository.dto;

import org.springframework.stereotype.Repository;
import ru.t1.sarychevv.tm.dto.model.ProjectDTO;

@Repository
public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

}

