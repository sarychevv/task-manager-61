package ru.t1.sarychevv.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.model.User;

public interface IUserService extends IService<User> {

    @NotNull
    User create(@Nullable final String login, @Nullable final String password);

    @NotNull
    User create(@Nullable final String login,
                @Nullable final String password,
                @Nullable final String email);

    @NotNull
    User create(@Nullable final String login,
                @Nullable final String password,
                @Nullable final Role role);

    @Nullable
    User findByLogin(@Nullable final String login);

    @Nullable
    User findByEmail(@Nullable final String email);

    Boolean isLoginExists(@Nullable final String login);

    Boolean isEmailExists(@Nullable final String email);

    void lockUserByLogin(@Nullable final String login);

    void removeByLogin(@Nullable final String login);

    void setPassword(@Nullable final String id,
                     @Nullable final String password);

    void unlockUserByLogin(@Nullable final String login);

    void updateUser(@Nullable final String id,
                    @Nullable final String firstName,
                    @Nullable final String lastName,
                    @Nullable final String middleName);

}

