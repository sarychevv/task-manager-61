package ru.t1.sarychevv.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;

public interface IProjectTaskDTOService {

    @Nullable
    TaskDTO bindTaskToProject(@Nullable String userId,
                              @Nullable String projectId,
                              @Nullable String taskId);

    @Nullable
    TaskDTO unbindTaskFromProject(@Nullable String userId,
                                  @Nullable String projectId,
                                  @Nullable String taskId);

    void removeProjectById(@Nullable String userId,
                           @Nullable String projectId);

}

