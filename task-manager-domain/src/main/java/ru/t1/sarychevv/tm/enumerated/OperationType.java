package ru.t1.sarychevv.tm.enumerated;

public enum OperationType {

    INSERT,
    UPDATE,
    DELETE

}
