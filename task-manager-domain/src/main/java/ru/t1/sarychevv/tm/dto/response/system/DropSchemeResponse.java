package ru.t1.sarychevv.tm.dto.response.system;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.response.AbstractResultResponse;

import javax.xml.bind.annotation.XmlType;

@NoArgsConstructor
@Getter
@Setter
@XmlType(name = "DropScheme")
public final class DropSchemeResponse extends AbstractResultResponse {

    public DropSchemeResponse(@Nullable final Throwable throwable) {
        super(throwable);
    }

}
