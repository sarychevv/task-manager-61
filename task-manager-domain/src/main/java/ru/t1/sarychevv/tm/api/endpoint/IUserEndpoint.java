package ru.t1.sarychevv.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.sarychevv.tm.dto.request.user.*;
import ru.t1.sarychevv.tm.dto.response.user.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint extends IEndpoint {

    @NotNull
    String NAME = "UserEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance() {
        return newInstance(HOST, PORT);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final IConnectionProvider connectionProvider) {
        return IEndpoint.newInstance(connectionProvider, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static IUserEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, SPACE, PART, IUserEndpoint.class);
    }

    @NotNull
    @WebMethod
    UserLockResponse lockUser(@WebParam(name = REQUEST, partName = REQUEST)
                              @NotNull UserLockRequest request) throws Exception;

    @NotNull
    @WebMethod
    UserUnlockResponse unlockUser(@WebParam(name = REQUEST, partName = REQUEST)
                                  @NotNull UserUnlockRequest request) throws Exception;

    @NotNull
    @WebMethod
    UserRemoveResponse removeUser(@WebParam(name = REQUEST, partName = REQUEST)
                                  @NotNull UserRemoveRequest request) throws Exception;

    @NotNull
    @WebMethod
    UserUpdateProfileResponse updateUserProfile(@WebParam(name = REQUEST, partName = REQUEST)
                                                @NotNull UserUpdateProfileRequest request) throws Exception;

    @NotNull
    @WebMethod
    UserChangePasswordResponse changeUserPassword(@WebParam(name = REQUEST, partName = REQUEST)
                                                  @NotNull UserChangePasswordRequest request) throws Exception;

    @NotNull
    @WebMethod
    UserRegistryResponse registryUser(@WebParam(name = REQUEST, partName = REQUEST)
                                      @NotNull UserRegistryRequest request) throws Exception;

}
