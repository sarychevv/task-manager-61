package ru.t1.sarychevv.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.endpoint.IAuthEndpoint;
import ru.t1.sarychevv.tm.api.endpoint.IUserEndpoint;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.dto.request.user.*;
import ru.t1.sarychevv.tm.marker.SoapCategory;

public class UserEndpointTest {

    @NotNull
    private final IUserEndpoint userEndpoint = IUserEndpoint.newInstance();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance();

    @Test
    @Category(SoapCategory.class)
    public void testLockUser() throws Exception {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("admin");
        userLoginRequest.setPassword("admin");
        @Nullable final String token = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserLockRequest userLockRequest = new UserLockRequest(token);
        userLockRequest.setLogin("user");
        @Nullable final UserDTO user = userEndpoint.lockUser(userLockRequest).getUser();
        Assert.assertEquals(true, user.getLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserUnlock() throws Exception {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("admin");
        userLoginRequest.setPassword("admin");
        @Nullable final String token = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserLockRequest userLockRequest = new UserLockRequest(token);
        userLockRequest.setLogin("user");
        userEndpoint.lockUser(userLockRequest);
        @NotNull final UserUnlockRequest userUnlockRequest = new UserUnlockRequest(token);
        userUnlockRequest.setLogin("user");
        Assert.assertFalse(userEndpoint.unlockUser((userUnlockRequest)).getUser().getLocked());
    }

    @Test
    @Category(SoapCategory.class)
    public void testUserRemove() throws Exception {
        @NotNull final UserLoginRequest userLoginRequest = new UserLoginRequest();
        userLoginRequest.setLogin("admin");
        userLoginRequest.setPassword("admin");
        @Nullable final String token = authEndpoint.login(userLoginRequest).getToken();
        @NotNull final UserRegistryRequest userRegistryRequest = new UserRegistryRequest(token);
        userRegistryRequest.setLogin("Test login");
        userRegistryRequest.setEmail("Test email");
        userRegistryRequest.setPassword("Test password");
        @NotNull final UserRemoveRequest userRemoveRequest = new UserRemoveRequest(token);
        userRemoveRequest.setLogin("Test login");
        Assert.assertTrue(userEndpoint.removeUser(userRemoveRequest).getSuccess());
    }


}
