package ru.t1.sarychevv.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import ru.t1.sarychevv.tm.api.endpoint.*;

@Configuration
@ComponentScan("ru.t1.sarychevv.tm")
public class ClientConfiguration {

    @NotNull
    @Bean
    public IAuthEndpoint authEndpoint() {
        return IAuthEndpoint.newInstance();
    }

    @NotNull
    @Bean
    public ISystemEndpoint systemEndpoint() {
        return ISystemEndpoint.newInstance();
    }

    @NotNull
    @Bean
    public IProjectEndpoint projectEndpoint() {
        return IProjectEndpoint.newInstance();
    }

    @NotNull
    @Bean
    public ITaskEndpoint taskEndpoint() {
        return ITaskEndpoint.newInstance();
    }

    @NotNull
    @Bean
    public IUserEndpoint userEndpoint() {
        return IUserEndpoint.newInstance();
    }

}
