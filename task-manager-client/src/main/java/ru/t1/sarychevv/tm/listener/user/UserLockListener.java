package ru.t1.sarychevv.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.user.UserLockRequest;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class UserLockListener extends AbstractUserListener {

    @NotNull
    @Override
    public String getDescription() {
        return "User lock";
    }

    @NotNull
    @Override
    public String getName() {
        return "user-lock";
    }

    @Override
    @EventListener(condition = "@UserLockListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[USER LOCK]");
        System.out.println("ENTER LOGIN:");
        @Nullable final String login = TerminalUtil.nextLine();
        @NotNull final UserLockRequest request = new UserLockRequest(getToken());
        request.setLogin(login);
        getUserEndpoint().lockUser(request);
    }

}
