package ru.t1.sarychevv.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.sarychevv.tm.dto.request.task.TaskChangeStatusByIdRequest;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.event.ConsoleEvent;
import ru.t1.sarychevv.tm.util.TerminalUtil;

@Component
public class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "Complete task by id.";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    @EventListener(condition = "@TaskCompleteByIdListener.getName == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(getToken());
        request.setId(id);
        request.setStatus(Status.COMPLETED);
        getTaskEndpoint().changeTaskStatusById(request);
    }

}
